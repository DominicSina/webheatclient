extends Camera

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var pressed=false
var old_pos=null

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass
	
func _input(event):
	# Mouse wheel camera zoom
	if (event is InputEventMouseButton and event.button_index==BUTTON_WHEEL_UP):
		translation=translation-translation.normalized()/10
	if (event is InputEventMouseButton and event.button_index==BUTTON_WHEEL_DOWN):
		translation=translation+translation.normalized()/10
		
	# Mouse Dragging to rotate around center
	if(event is InputEventMouseButton and event.button_index==BUTTON_LEFT):
		pressed = event.is_pressed()
	if (event is InputEventMouseMotion):
		if (pressed):
			var mouse_position_moved = event.position
			
			if(old_pos!=null):
				var pos_diff=old_pos-mouse_position_moved
				
				var vp_size=get_viewport().get_size()
				
				print("Mouse position at :" + String(mouse_position_moved))
				transform=transform.rotated( Vector3(0,1,0), ((2*PI)*pos_diff.x/vp_size.x))
				transform=transform.rotated( translation.cross(Vector3(0,1,0)).normalized(),
							 -((2*PI)*pos_diff.y/vp_size.y))
							
			old_pos=mouse_position_moved
		else:
			old_pos=null
			pressed=false
			
	pass
	
