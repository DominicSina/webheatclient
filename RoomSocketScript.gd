extends Node

var tcp = StreamPeerTCP.new()
var peer = PacketPeerStream.new()
var _conn = false

func _ready():
	_log("WebSocket Started!")
	tcp.connect_to_host("echo.websocket.org", 80)
	_log("WebSocket Connecting...")
	set_process(true)

func _process(delta):
	if tcp.get_status() == StreamPeerTCP.STATUS_CONNECTING:
		# still connecting, nothing to do
		pass
	elif tcp.get_status() == StreamPeerTCP.STATUS_CONNECTED:
		if _conn == false:
			_log("WebSocket Connected!")
			peer.set_stream_peer(tcp)
			_conn = true
			peer.put_var("WebSocket rocks")
		if peer.get_available_packet_count() > 0:
			var pkt = peer.get_packet()
			_output("Got packet: " + str(Array(pkt)))
			_output("Packet text: " + str(bytes2var(pkt)))
			tcp.disconnect_from_host()
			set_process(false);
		return
	elif _conn:
		_conn = false
		_log("Disconnected from server with status: " + str(tcp.get_status()))
		if peer.get_available_packet_count() > 0:
			var pkt = peer.get_packet()
			_output("Got packet: " + str(Array(pkt)))
			_output("Packet text: " + str(bytes2var(pkt)))
	else:
		_log("Unable to connect, status: " + str(tcp.get_status()))
		tcp.disconnect_from_host()
		set_process(false)

func _output(msg):
	print(str(msg) + "\n")

func _log(msg):
	print(str(msg) + "\n")

#func _on_Button_pressed():
#	if not _conn:
#		_log("Cannot send, not connected")
#	else:
#		_log("Sending: " + get_node("Control/Panel/VLayout/HLayout/LineEdit").text)
#		peer.put_packet(var2bytes(get_node("Control/Panel/VLayout/HLayout/LineEdit").text))
#get_node("Control/Panel/VLayout/HLayout/LineEdit").clear()