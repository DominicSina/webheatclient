extends ImmediateGeometry

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	print("readyGeometry")
	# Called every time the node is added to the scene.
	# Initialization here
	begin(PrimitiveMesh.PRIMITIVE_TRIANGLES)
	#print("beginGeometry")
	#var requestNode=get_node("HTTPRequest")
	#var response=requestNode.response["headers"]
	#print(response.keys())
	
	set_color(Color(1.0,1.0,0.0))
	#print("setcolorGeometry")
	#for i in range(2):
	#	add_vertex(Vector3(0,i,0.1))
	#	add_vertex(Vector3(i,0,0.1))
	#	add_vertex(Vector3(0,-i,0.1))
	#set_color(Color(0.0,0.0,0.0))
	add_sphere(2,2,1,true)
	#print("drawnGeometry")
	end()
	#print("endGeometry")